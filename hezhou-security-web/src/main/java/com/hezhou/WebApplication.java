package com.hezhou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ---------------------------
 * (ReqDesignateWorkOrder) 启动类
 * ---------------------------
 *
 * @Author: [hezhou]
 * @Date: 2020/2/26
 * @Version: [1.0.1]
 * ---------------------------
 */
@SpringBootApplication
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class,args);
    }
}
