# MY_SPRING_SECURYTY

#### 介绍
这是我自己搭的一个简单整合spring_security的项目 
1.自定义登录，注册
2.包括手机验证码登录，用户名密码登录
3.自定义短信登陆的一些过滤器链  实现验证码的校验(目前没有短信服务 所以先模拟)
4.系统退出 已经设置一台机器在不通浏览器中重登的问题
5.实现记住我功能 redis缓存Session共享信息

#### 软件架构
软件架构说明
使用springboot单体架构 整合springsecurity


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
